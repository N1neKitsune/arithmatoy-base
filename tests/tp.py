# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    return 'S' * n + '0'
    #return f'{"S" * n}0'

def S(n: str) -> str:
    #return 'S' + n
    return f"S{n}"

def addition(a: str, b: str) -> str:
    if a == '0':
        return b
    else:
        return S(addition(a[1:], b))


def multiplication(a: str, b: str) -> str:
    if a == '0':
        return '0'
    else :
        return addition(multiplication(a[1:],b),b)


def facto_ite(n: int) -> int:
    i = 1
    result = 1
    while i <= n :
        result *= i
        i += 1
    return result


def facto_rec(n: int) -> int:
    if n == 0:
        return 1
    else:
        return n * facto_rec(n-1)


def fibo_rec(n: int) -> int:
    if n <= 1:
        return n
    else:
        return fibo_rec(n-1) + fibo_rec(n-2)


def fibo_ite(n: int) -> int:
    a,b = [0,1]
    if n <= 1:
        return n
    elif n == 2:
        return b
    else:
        for i in range(n):
            a,b = b,a+b
        return a


def golden_phi(n: int) -> int:
    return fibo_ite(n + 1) / fibo_ite (n)


def sqrt5(n: int) -> int:
    return 2 * golden_phi(n) - 1



def pow(a: float, n: int) -> float:
    if n == 0 :
        return 1
    res = pow(a, n // 2)
    res *= res
    if n % 2 == 0 :
        return res
    return res * a
